#![no_std]
#![no_main]

use stm32f1xx_hal::{pac, prelude::*};

use defmt_rtt as _;
use panic_probe as _;

#[defmt_test::tests]
mod tests {
    use super::*;

    struct State {
        led: stm32f1xx_hal::gpio::ErasedPin<
            stm32f1xx_hal::gpio::Output<stm32f1xx_hal::gpio::PushPull>,
        >,
    }

    use defmt::assert;
    #[init]
    fn setup() -> State {
        let dp = pac::Peripherals::take().unwrap();

        let mut flash = dp.FLASH.constrain();
        let rcc = dp.RCC.constrain();
        rcc.cfgr.freeze(&mut flash.acr);

        let mut gpioc = dp.GPIOC.split();
        let led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh).erase();

        State { led }
    }

    #[test]
    fn do_test() {
        assert!(true)
    }

    #[test]
    fn light_on(state: &mut State) {
        state.led.set_high();
    }

    #[test]
    fn light_off(state: &mut State) {
        state.led.set_low();
    }

    #[test]
    fn do_fail() {
        assert!(false)
    }
}
