#![no_std]
#![no_main]

use nb::block;
use stm32f1xx_hal::{pac, prelude::*, timer::Timer};

use defmt::info;
use defmt_rtt as _;
use panic_probe as _;
//use panic_halt as _;

#[cortex_m_rt::entry]
fn entry() -> ! {
    let dp = pac::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let rcc = dp.RCC.constrain();
    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    let mut gpioc = dp.GPIOC.split();
    let mut led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);

    let cp = cortex_m::Peripherals::take().unwrap();
    let mut timer = Timer::syst(cp.SYST, &clocks).start_count_down(1.hz());
    loop {
        info!("Blink!");
        block!(timer.wait()).unwrap();
        led.set_high();
        block!(timer.wait()).unwrap();
        led.set_low();
    }
}
